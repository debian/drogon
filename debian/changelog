drogon (1.9.0+ds-2) unstable; urgency=medium

  * Fix changelog and add missing lintian override.

 -- Pierre-Elliott Bécue <peb@debian.org>  Sat, 24 Aug 2024 00:44:07 +0200

drogon (1.9.0+ds-1) unstable; urgency=medium

  * New upstream release 1.9.0+ds
  * d/control:
    - Add missing Build-Dependency on libc-ares-dev (Closes: #1071304)
    - Bump Standards-Version to 4.7.0
    - Change Maintainer
    - Fix Sections and descriptions for both the lib and dev package
    - Add t64 suffix to libdrogon1
  * d/copyright: Updated
  * d/rules: Set hardening flags

 -- Pierre-Elliott Bécue <peb@debian.org>  Fri, 23 Aug 2024 19:14:19 +0200

drogon (1.8.7+ds-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062387

 -- Michael Hudson-Doyle <mwhudson@debian.org>  Wed, 28 Feb 2024 02:24:07 +0000

drogon (1.8.7+ds-1) unstable; urgency=medium

  * Drop .github and .gitmodules from upstream source
  * New upstream release 1.8.7+ds
  * Fix typo on copyright path

 -- Pierre-Elliott Bécue <pierre-elliott.becue@gandi.net>  Thu, 26 Oct 2023 21:16:37 +0200

drogon (1.8.4+ds-2) unstable; urgency=medium

  * Add lib/inc/drogon/version.h to debian/clean (Closes: #1044560)
  * Add a debian/watch file

 -- Pierre-Elliott Bécue <pierre-elliott.becue@gandi.net>  Wed, 13 Sep 2023 16:00:57 +0200

drogon (1.8.4+ds-1) unstable; urgency=medium

  * Initial release (Closes: #1037495)
  * Repack to drop third_party directory

 -- Pierre-Elliott Bécue <peb@debian.org>  Mon, 26 Jun 2023 22:27:40 +0200
